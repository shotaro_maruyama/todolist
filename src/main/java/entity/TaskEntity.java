package entity;

import java.util.Date;

public class TaskEntity {
	private int id; //編集・削除用
	private String task;
	private Date start_Date;
	private Date finish_Date;

//	private String start_Hour;
//	private String start_Minute;
//
//	private String finish_Hour;
//	private String finish_Minute;

	private int is_Finished;

	public int getIs_Finished() {
		return is_Finished;
	}
	public void setIs_Finished(int is_Finished) {
		this.is_Finished = is_Finished;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getTask() {
		return task;
	}
	public void setTask(String task) {
		this.task = task;
	}
	public Date getStart_Date() {
		return start_Date;
	}
	public void setStart_Date(Date start_Date) {
		this.start_Date = start_Date;
	}
	public Date getFinish_Date() {
		return finish_Date;
	}
	public void setFinish_Date(Date finish_Date) {
		this.finish_Date = finish_Date;
	}
//	public String getStart_Hour() {
//		return start_Hour;
//	}
//	public void setStart_Hour(String start_Hour) {
//		this.start_Hour = start_Hour;
//	}
//	public String getStart_Minute() {
//		return start_Minute;
//	}
//	public void setStart_Minute(String start_Minute) {
//		this.start_Minute = start_Minute;
//	}
//	public String getFinish_Hour() {
//		return finish_Hour;
//	}
//	public void setFinish_Hour(String finish_Hour) {
//		this.finish_Hour = finish_Hour;
//	}
//	public String getFinish_Minute() {
//		return finish_Minute;
//	}
//	public void setFinish_Minute(String finish_Minute) {
//		this.finish_Minute = finish_Minute;
//	}


}
