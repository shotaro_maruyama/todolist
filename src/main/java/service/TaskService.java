package service;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import dto.TaskDto;
import entity.TaskEntity;
import form.TaskForm;
import mapper.TaskMapper;

@Service
public class TaskService {

	@Autowired
    private TaskMapper taskMapper;

	public List<TaskForm> getTaskAll() {
	    List<TaskEntity> taskList = taskMapper.getTaskAll(); //この行でDao呼び出し
	    List<TaskDto> resultDto = convertToListDto(taskList);
	    List<TaskForm> resultForm = convertToListForm(resultDto);
	    return resultForm;

	}

	public void deleteService(TaskForm form) {
		int id = form.getId();
	    taskMapper.deleteTask(id);
	}

	public TaskForm getDisignatedTask(TaskForm taskForm) {
		int id = taskForm.getId();
		TaskEntity task = new TaskEntity();

	    task  = taskMapper.getTask(id);

	    TaskDto dto = convertToString(task);
	    TaskForm form = new TaskForm();
        BeanUtils.copyProperties(dto, form);
	    return form;
	}

	public void insertTask(TaskForm taskForm) {
		TaskEntity taskEntity = new TaskEntity();
		TaskDto convertedDto =convertToDto(taskForm);
		BeanUtils.copyProperties(convertedDto, taskEntity);
		taskMapper.insertTask(taskEntity);
	}

	public void updateTask(TaskForm taskForm) {
		TaskEntity taskEntity = new TaskEntity();
		TaskDto convsertedDto = convertToDto(taskForm);
		BeanUtils.copyProperties(convsertedDto, taskEntity);
		taskMapper.updateTask(taskEntity);
	}

	public void switchIsFinished(TaskForm form) {
		TaskEntity switchIsFinisedEntity = new TaskEntity();
		BeanUtils.copyProperties(form, switchIsFinisedEntity);
		taskMapper.switchIsFinished(switchIsFinisedEntity);
	}

	public TaskDto convertToString(TaskEntity taskEntity) {
		TaskDto convertedDto = new TaskDto();
		BeanUtils.copyProperties(taskEntity, convertedDto);

		String[] splitedStartTimeStamp =  new  SimpleDateFormat("yyyy-MM-dd H:mm").format(taskEntity.getStart_Date()).split(" ");
		String[] splitedFinishTimeStamp = new  SimpleDateFormat("yyyy-MM-dd H:mm").format(taskEntity.getFinish_Date()).split(" ");
//		splitedStartDate[0]=yyyy-MM-dd splitedStartDate[1]= hh:mm
//		splitedFinishDate[0]=yyyy-MM-dd splitedStartDate[1]= hh:mm
		String splitedStartTime[] = splitedStartTimeStamp[1].split(":");
		String splitedFinishTime[] = splitedFinishTimeStamp[1].split(":");
//		startTime[0] = hh startTime[1]=mm
//		finishTime[0] =hh finishTime[1]=mm

		convertedDto.setStrStart_Date(splitedStartTimeStamp[0]);
		convertedDto.setStart_Hour(splitedStartTime[0]);
		convertedDto.setStart_Minute( splitedStartTime[1]);
		convertedDto.setStrFinish_Date(splitedFinishTimeStamp[0]);
		convertedDto.setFinish_Hour(splitedFinishTime[0]);
		convertedDto.setFinish_Minute(splitedFinishTime[1]);

		return convertedDto;
	}

	private List<TaskDto> convertToListDto(List<TaskEntity> taskList) {
		List<TaskDto> resultList = new LinkedList<TaskDto>();
		for (TaskEntity taskEntity : taskList) {
			TaskDto dto = new TaskDto();
			BeanUtils.copyProperties(taskEntity, dto);
			resultList.add(dto);
		}
		return resultList;
	}

	private List<TaskForm> convertToListForm(List<TaskDto> taskList) {
	    List<TaskForm> resultList = new LinkedList<TaskForm>();
	    for (TaskDto taskDto : taskList) {
	        TaskForm form = new TaskForm();
	        BeanUtils.copyProperties(taskDto, form);
	        resultList.add(form);
	    }
	    return resultList;
	}

	/*
	 * インサートかアップデートのみ呼ばれる
	 */
	public TaskDto convertToDto(TaskForm taskForm) {
		TaskDto convertedDto = new TaskDto();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm");

		String jointedStartDate = taskForm.getStrStart_Date() +" "+ taskForm.getStart_Hour()+":"+taskForm.getStart_Minute();
		String jointedFinishDate = taskForm.getStrFinish_Date() +" "+taskForm.getFinish_Hour()+":"+taskForm.getFinish_Minute();
		System.out.println();
		try {
			BeanUtils.copyProperties(taskForm, convertedDto);
			Date startDate = sdf.parse(jointedStartDate);
			Date finishDate = sdf.parse(jointedFinishDate);
			convertedDto.setStart_Date(startDate);
			convertedDto.setFinish_Date(finishDate);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return convertedDto;
	}

	public String[] creartTimeFormat(String[] timeForm) {

		if(timeForm.length == 12) {
			for(int i=0; i <= 11; i++) {
				if((i*5)<10) {
					timeForm[i] ="0"+String.valueOf(i*5);
				}else {
					timeForm[i] =String.valueOf(i*5);
				}
			}
			return timeForm;
		}else {
			for(int i=0; i <= 23; i++) {
				if(i<10) {
					timeForm[i] ="0"+String.valueOf(i);
				}else {
					timeForm[i] =String.valueOf(i);
				}
			}
			return timeForm;
		}
	}

}