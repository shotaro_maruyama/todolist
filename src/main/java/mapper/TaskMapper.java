package mapper;

import java.util.List;

import entity.TaskEntity;

public interface TaskMapper {

	public abstract TaskEntity getTask(int id);
	public abstract TaskEntity deleteTask(int id);
	public abstract void updateTask(TaskEntity taskEntity);
	public abstract List<TaskEntity> getTaskAll();
	public abstract void insertTask(TaskEntity taskEntity);
	public abstract void switchIsFinished(TaskEntity switchIsFinisedEntity);


}
