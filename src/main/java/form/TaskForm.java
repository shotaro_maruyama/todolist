package form;

import java.util.Date;

import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotEmpty;

public class TaskForm {
	private int id; //編集・削除用
	@NotEmpty
	@Size(max = 30)
	private String task;
	private Date start_Date;
	private Date finish_Date;
	private String strStart_Date;
	private String strFinish_Date;

	private String start_Hour;
	private String start_Minute;

	private String finish_Hour;
	private String finish_Minute;

	private int is_Finished;




	@Override
	public String toString() {
		return "TaskForm [id=" + id + ", task=" + task + ", start_Date=" + start_Date + ", finish_Date=" + finish_Date
				+ ", strStart_Date=" + strStart_Date + ", strFinish_Date=" + strFinish_Date + ", start_Hour="
				+ start_Hour + ", start_Minute=" + start_Minute + ", finish_Hour=" + finish_Hour + ", finish_Minute="
				+ finish_Minute + ", is_Finished=" + is_Finished + "]";
	}
	public String getStrStart_Date() {
		return strStart_Date;
	}
	public void setStrStart_Date(String strStart_Date) {
		this.strStart_Date = strStart_Date;
	}
	public String getStrFinish_Date() {
		return strFinish_Date;
	}
	public void setStrFinish_Date(String strFinish_Date) {
		this.strFinish_Date = strFinish_Date;
	}

	public int getIs_Finished() {
		return is_Finished;
	}
	public void setIs_Finished(int is_Finished) {
		this.is_Finished = is_Finished;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getTask() {
		return task;
	}
	public void setTask(String task) {
		this.task = task;
	}
	public Date getStart_Date() {
		return start_Date;
	}
	public void setStart_Date(Date start_Date) {
		this.start_Date = start_Date;
	}
	public Date getFinish_Date() {
		return finish_Date;
	}
	public void setFinish_Date(Date finish_Date) {
		this.finish_Date = finish_Date;
	}
	public String getStart_Hour() {
		return start_Hour;
	}
	public void setStart_Hour(String start_Hour) {
		this.start_Hour = start_Hour;
	}
	public String getStart_Minute() {
		return start_Minute;
	}
	public void setStart_Minute(String start_Minute) {
		this.start_Minute = start_Minute;
	}
	public String getFinish_Hour() {
		return finish_Hour;
	}
	public void setFinish_Hour(String finish_Hour) {
		this.finish_Hour = finish_Hour;
	}
	public String getFinish_Minute() {
		return finish_Minute;
	}
	public void setFinish_Minute(String finish_Minute) {
		this.finish_Minute = finish_Minute;
	}


}
