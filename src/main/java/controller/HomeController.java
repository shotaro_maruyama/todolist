package controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import form.TaskForm;
import service.TaskService;

@Controller
public class HomeController {

	@Autowired
    private TaskService taskService;

	@RequestMapping(value = "/home", method = RequestMethod.GET)
	public String taskAll(Model model) {
	    List<TaskForm> allTask = taskService.getTaskAll();
	    TaskForm blankForm = new TaskForm();
	    String[] hours = new String[24];
		String[] minutes = new String[12];
		hours = taskService.creartTimeFormat(hours);
		minutes = taskService.creartTimeFormat(minutes);
	    model.addAttribute("hours", hours);
	    model.addAttribute("minutes", minutes);
	    model.addAttribute("allTask", allTask);
	    model.addAttribute("finishTask", blankForm);
	    model.addAttribute("editTaskButton", blankForm);
	    model.addAttribute("deleteTaskButton", blankForm);
	    model.addAttribute("taskForm", blankForm);
	    return "home";
	}

	@RequestMapping(value = "/home/register", method = RequestMethod.POST)
	public String taskInsert(@Valid @ModelAttribute TaskForm taskForm, BindingResult result, Model model,
			RedirectAttributes redirectAttributes) {

		if (result.hasErrors()) {
			List<TaskForm> allTask = taskService.getTaskAll();
		    TaskForm blankForm = new TaskForm();
		    String[] hours = new String[24];
			String[] minutes = new String[12];
			hours = taskService.creartTimeFormat(hours);
			minutes = taskService.creartTimeFormat(minutes);
		    model.addAttribute("hours", hours);
		    model.addAttribute("minutes", minutes);
		    model.addAttribute("allTask", allTask);
		    model.addAttribute("finishTask", blankForm);
		    model.addAttribute("editTaskButton", blankForm);
		    model.addAttribute("deleteTaskButton", blankForm);
		    model.addAttribute("allTask", allTask);
			return "home";

	    } else {
	    	taskService.insertTask(taskForm);
	        return "redirect:/home";
	    }
	}

	@RequestMapping(value = "/home/finish", method = RequestMethod.POST)
	public String switchTaskIsFinished(@ModelAttribute TaskForm form, Model model) {
	    taskService.switchIsFinished(form);
	    return "redirect:/home";
	}

	@RequestMapping(value = "/home/delete", method = RequestMethod.POST)
	public String deleteTask(@ModelAttribute TaskForm form, Model model) {
	    taskService.deleteService(form);
	    return "redirect:/home";
	}

	@RequestMapping(value = "/home/edit", method = RequestMethod.POST)
	public String editTask(@Valid @ModelAttribute TaskForm taskForm, BindingResult result, Model model,
			RedirectAttributes redirectAttributes) {
		if (result.hasErrors()) {
			redirectAttributes.addFlashAttribute("taskErrorMessage", "タスクを入力してください");
			redirectAttributes.addFlashAttribute("taskForm", taskForm);
			return "redirect:/home/edit";
	    } else {
	    	taskService.updateTask(taskForm);
	        return "redirect:/home";
	    }
	}

	@RequestMapping(value = "/home/edit", method = RequestMethod.GET)
	public String goToEditDisp(@ModelAttribute TaskForm form, Model model) {
		String[] hours = new String[24];
		hours = taskService.creartTimeFormat(hours);

		String[] minutes = new String[12];
		minutes = taskService.creartTimeFormat(minutes);
	    TaskForm extractedForm = taskService.getDisignatedTask(form);
	    model.addAttribute("taskForm", extractedForm);
	    model.addAttribute("hours", hours);
	    model.addAttribute("minutes", minutes);
	    return "editcontents";
	}
}


//		List<String> minutes = new ArrayList<String>();
//		for(int i = 0; i <= 11 ; i++) {
//			minutes.add(String.valueOf(i*5));
//        }