	$(function (){

		var startDate =$("#strStart_Date").val()+$("#start_Hour").val()+$("#start_Minute").val();
		startDate = startDate.replace(/-/g,'');
		var finishDate =$("#strFinish_Date").val()+$("#finish_Hour").val()+$("#finish_Minute").val();
		finishDate = finishDate.replace(/-/g,'');

		var hourArray = $('#start_Hour').children();
		var minuteArray = $('#start_Minute').children();
		var date = new Date();
		var year = date.getFullYear() +"-" ;
		var month = (date.getMonth() +1);
			if(month < 10){month  = "0"+ month + "-"}else{month = month +"-"}
		var day = date.getDate();
			if(day < 10){day = "0" + day}

		var today = year + month + day


		if($('#task').val() == ''){
			$('#taskSubmit').prop('disabled',true);
		}else if(parseInt(startDate,10)<parseInt(finishDate,10)){
			$('#taskSubmit').prop('disabled',false);
		}else if(parseInt(startDate,10)>parseInt(finishDate,10)){
			$('#taskSubmit').prop('disabled',true);
		}else if(parseInt(startDate,10)==parseInt(finishDate,10)){
			$('#taskSubmit').prop('disabled',false);
		}

		$(".DeleteButton").on("click", function() {
			if(window.confirm('削除してよろしいですか？')){
				return true;
			}
			else{
				return false;
			}
		});

		$('#defaultTimeCheckBox').change(function() {

			if($('#defaultTimeCheckBox:checked').val()=='on'){

				$('#start_Hour > option').remove();
				$('#start_Hour').append($('<option>').html("00"));

				$('#start_Minute > option').remove();
				$('#start_Minute').append($('<option>').html("00"));

				$('#finish_Hour > option').remove();
				$('#finish_Hour').append($('<option>').html("23"));

				$('#finish_Minute > option').remove();
				$('#finish_Minute').append($('<option>').html("59"));

				$('input[id$="Date"]').val(today);
				console.log(today)


			}else{

				$('input[id$="Date"]').val("");

				$('#start_Hour > option').remove();
				$('#start_Minute > option').remove();
				$('#finish_Hour > option').remove();
				$('#finish_Minute > option').remove();


				$.each(hourArray,function(index, value){
					$('select[id$="Hour"]').append($('<option>').html(value));
					/*$('#finish_Hour').append($('<option>').html(value));*/
				});

				$.each(minuteArray,function(index, value){
					$('select[id$="Minute"]').append($('<option>').html(value));
				});

			}

		});

	});

	$(function (){
		$('#taskForm').find('input,select').change(function() {

			var startDate =$("#strStart_Date").val()+$("#start_Hour").val()+$("#start_Minute").val();
			startDate = startDate.replace(/-/g,'');
			var finishDate =$("#strFinish_Date").val()+$("#finish_Hour").val()+$("#finish_Minute").val();
			finishDate = finishDate.replace(/-/g,'');

			if($('#task').val() == ''){
				$('#taskSubmit').prop('disabled',true);
				$("#JsValidateMessage").text("タスクが未入力です");

			}else if($('#strStart_Date').val() == ''){
				$('#taskSubmit').prop('disabled',false);
				$("#JsValidateMessage").text("日程が未入力です");
			}else if($('#strFinish_Date').val() == ''){
				$('#taskSubmit').prop('disabled',false);
				$("#JsValidateMessage").text("日程が未入力です");
			}else if(parseInt(startDate,10)<parseInt(finishDate,10)){
				$('#taskSubmit').prop('disabled',false);
				$("#JsValidateMessage").text("")
			}else if(parseInt(startDate,10)>parseInt(finishDate,10)){
				$('#taskSubmit').prop('disabled',true);
				$("#JsValidateMessage").text("開始日は終了日より前である必要があります。")
			}else if(parseInt(startDate,10)==parseInt(finishDate,10)){
				$('#taskSubmit').prop('disabled',false);
				$("#JsValidateMessage").text("")
			}

		});

	});