<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>タスク編集</title>
<link href="<c:url value="/resources/css/editcontents.css" />" rel="stylesheet">
 <script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
        <script type="text/javascript" src="<c:url value="/resources/js/editcontents.js" />"></script>
</head>
<body>
<jsp:include page="/WEB-INF/view/layout/header.jsp"/>
<a href="http://localhost:8080/ToDo/home">ホーム</a>
<span id="TaskErrorMessage"><form:errors path="*"  /></span><br>
	<div id = "TaskForm">
	<h3>タスク編集</h3>
	<form:form modelAttribute="taskForm" action="/ToDo/home/edit" method="post">
	<ul id="ItemHeader">
		<li id="TaskHeader">タスク(30字以内)</li>
		<li id="StartDayHeader">開始日</li>
		<li id="StartTimeHeader">開始時間</li>
		<li id="FinishDayHeader">終了日</li>
		<li id="FinishTimeHeader">終了時間</li>
	</ul>
			<form:input path="task" />
			<form:input type="date" path="strStart_Date" />

			 <form:select items="${hours}" path="start_Hour"/>

			 <form:select path="start_Minute">
				<form:options items="${minutes}"/>
			</form:select>

			<form:input type="date" path="strFinish_Date" />

			<form:select path="finish_Hour">
			<form:options items="${hours}"/>
			</form:select>

			<form:select path="finish_Minute">
			<form:options items="${minutes}"/>
			</form:select>

			<form:hidden path="id" value="${taskForm.id}"/>
			<input type="submit" id ="taskSubmit"><br>
			<span id="TaskErrorMessage"><form:errors path="*"  /></span>
			<span id="JsValidateMessage" ></span>
		</form:form>
		</div>
		<jsp:include page="/WEB-INF/view/layout/footer.jsp"/>
</body>
</html>