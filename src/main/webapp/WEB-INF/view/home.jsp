<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<html>
    <head>
        <meta charset="utf-8">
        <title>ToDoリスト</title>
        <link href="<c:url value="/resources/css/home.css" />" rel="stylesheet">
        <script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
        <script type="text/javascript" src="<c:url value="/resources/js/todo.js" />"></script>
    </head>
    <body>
    <jsp:include page="/WEB-INF/view/layout/header.jsp"/>

<div id ="TwoColumn">
    <div id = "setting">
    <ul id="menu">
		<li>並び替え</li>
		<li><label for="defaultTime"><span id ="dafaultTimeLabel">予定時間を本日に設定</span></label>
			<input type="checkbox" id="defaultTimeCheckBox" name="defaultTime"></li>
		<li>coming soon</li>
		<li>coming soon</li>
		<li>coming soon</li>
	</ul>
    </div>
    <div id ="main">
    <h2>やることリスト</h2>

    	<p id ="ErrorMessage"><c:out value="${errorMessage}"></c:out></p>

			<div id="Tasks">

	        <table class = "Task">
	        <tr id ="ItemField">
	        <th>タスク</th>
	        <th class ="date">開始予定</th>
	        <th class ="date">終了予定</th>
	        <th>状態</th>
	        <th></th>
	        </tr>
	        <c:forEach items="${allTask}" var="task">
		        	<tr id="Field1">

			            <td id = "TaskCell"><c:out value="${task.task}"></c:out></td>
			            <td class ="date"><fmt:formatDate value="${task.start_Date}" pattern="MM/dd HH:mm" /></td>
			            <td class ="date"><fmt:formatDate value="${task.finish_Date}" pattern="MM/dd HH:mm" /></td>

				            <c:if test="${task.is_Finished == 0}">
								<td>未完</td>
							</c:if>
							<c:if test="${task.is_Finished == 1}">
								<td>完了</td>
							</c:if>

						    <form:form modelAttribute="deleteTaskButton" action="/ToDo/home/delete" >
								<form:hidden path="id" value="${task.id}"/>
							    <td class = "DeleteButton"><input type="submit" value="削除"/></td>
						    </form:form>
					</tr>

					<tr id="Field2">
						<td></td>
						<td></td>
						<td></td>

							<td class = "Button">
						    <form:form modelAttribute="editTaskButton" action="/ToDo/home/edit" method="get">
								<form:hidden path="id" value="${task.id}"/>
							    <input type="submit" value="編集"/>
						    </form:form>

			            	<form:form modelAttribute="finishTask" action="/ToDo/home/finish">
			            		<c:if test="${task.is_Finished == 0}">
								    <form:hidden path="is_Finished" value="1"/>
								    <form:hidden path="id" value="${task.id}"/>
								    <input type="submit" value="完了"/>
							    </c:if>

							    <c:if test="${task.is_Finished == 1}">
								    <form:hidden path="is_Finished" value="0"/>
								    <form:hidden path="id" value="${task.id}"/>
								   <input type="submit" value="未完に戻す"/>
							    </c:if>
							</form:form>
							</td>

						<td></td>
					</tr>
	        </c:forEach>
			</table>
	        </div>

				<div id="TaskForm">
				<h3>タスク登録</h3>
		        <form:form modelAttribute="taskForm" action="/ToDo/home/register" method="POST">
				<ul id="ItemHeader">
					<li id="TaskHeader">タスク(30字以内)</li>
					<li id="StartDayHeader">開始日</li>
					<li id="StartTimeHeader">開始時間</li>
					<li id="FinishDayHeader">終了日</li>
					<li id="FinishTimeHeader">終了時間</li>
				</ul>
					<form:input path="task" data-target="taskInputBox"/>
					<form:input type="date" path="strStart_Date" />

					<form:select path="start_Hour" >
					<form:options items="${hours}"/>
					</form:select>

					<form:select path="start_Minute">
					<form:options items="${minutes}"/>
					</form:select>

					<form:input type="date" path="strFinish_Date" />
					<form:select path="finish_Hour">
					<form:options items="${hours}"/>
					</form:select>

					<form:select path="finish_Minute">
					<form:options items="${minutes}"/>
					</form:select>
					<input type="submit" value="登録" id ="taskSubmit"><br>
					<span id="TaskErrorMessage"><form:errors path="*"  /></span>
					<span id="JsValidateMessage" ></span>
				</form:form>
				</div>
			</div>
		</div>
		<jsp:include page="/WEB-INF/view/layout/footer.jsp"/>
    </body>
</html>